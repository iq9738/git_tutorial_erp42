#include "steer.h"

int main(int argc, char* argv[]){

    auto steer = steer_interface::get_steer();
    steer_interface::set_steer(0.0);

    return 0;
}
